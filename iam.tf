
# Proxy for Aurora

resource "aws_iam_role" "db_proxy_policy" {
  name = "rds-${local.datastore_db_name}-user-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "rds.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "db_proxy_policy" {
  name        = "${local.datastore_db_name}-policy-db-proxy"
  path        = "/"
  description = "IAM policy for logging into the aurora db"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowGetSecret",
      "Action": [
                "secretsmanager:GetResourcePolicy",
                "secretsmanager:GetSecretValue",
                "secretsmanager:DescribeSecret",
                "secretsmanager:ListSecretVersionIds"
      ],
      "Effect": "Allow",
      "Resource": "${aws_secretsmanager_secret.rds_username_and_password.arn}"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "test-attach" {
  name       = "role-proxy-attachment"
  roles      = [aws_iam_role.db_proxy_policy.name]
  policy_arn = aws_iam_policy.db_proxy_policy.arn
}