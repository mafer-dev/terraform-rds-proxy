# RDS Proxy With Aurora DB

## Variables para la IaC
Crear un archivo con el nombre "variables.tf" y settear los valores de las variables:

```js
variable "aws_region" {
    type = string
    default = "us-east-1"
}

variable "vpc_cidr" {
    type = string
    default = "192.168.0.0/16"
}

variable "vpc_name" {
    type = string
    default = "vpc_practice1"
}

variable "subnet_cidr" {
    type = list(string)
    default = ["192.168.1.0/24","192.168.2.0/24","192.168.3.0/24"]
}

variable "az" {
    type = list(string)
    default = ["us-east-1a","us-east-1b","us-east-1c"]
}

variable "subnet_name" {
    type = list(string)
    default = ["Subnet1a","Subnet1b","Subnet1c"]
}

variable "user_db_service_pe" {
    type = string
    default = "superuser"
}
variable "password_db_service_pe" {
    type = string
    default = "admin123"
}

variable "datastore_db_name" {
    type = string
    default = "service-aurora-datastore"
}

```
