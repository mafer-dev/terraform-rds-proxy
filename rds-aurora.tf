resource "aws_rds_cluster_parameter_group" "pe_parameter_group" {
  name = "${local.datastore_db_name}-pg"
  family = "aurora-mysql5.7"
  tags = {
      "Name" = "${local.datastore_db_name}-pg"
      "nombre" = "${local.datastore_db_name}-pg"
    }
}

resource "aws_rds_cluster" "pe_aurora_network" {
  cluster_identifier = local.datastore_db_name
  engine = "aurora-mysql"
  engine_version = "5.7.mysql_aurora.2.07.2"
  # engine_mode = "serverless"

  # Database
  database_name = null
  master_username = var.user_db_service_pe   //aws_rds_cluster
  master_password = var.password_db_service_pe

  # # Backups
  # backup_retention_period = 7
  # final_snapshot_identifier = local.datastore_db_name

  # scaling_configuration {
  #   max_capacity = 32
  #   min_capacity = 2
  # }

  # Networking
  db_subnet_group_name = aws_db_subnet_group.pe_aurora_subnet.name
  vpc_security_group_ids = [module.pe_aurora_network_sg.this_security_group_id]
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.pe_parameter_group.name
  enable_http_endpoint = true
  port = 3306
  availability_zones = [
    var.az[0],
    var.az[1],
  ]

  tags = {
      "Name" = local.datastore_db_name
      "nombre" = local.datastore_db_name
    }
}