resource "random_password" "admin_password" {
count  = var.password_db_service_pe == "" || var.password_db_service_pe == null ? 1 : 0
  length           = 33
  special          = false
  override_special = "!#$%^&*()<>-_"
}

locals {
  database_password = var.password_db_service_pe != "" && var.password_db_service_pe != null ? var.password_db_service_pe : join("", random_password.admin_password.*.result)

  username_password = {
    username = var.user_db_service_pe
    password = local.database_password
  }

  auth = [
    {
      auth_scheme = "SECRETS"
      description = "Access the database instance using username and password from AWS Secrets Manager"
      iam_auth    = "DISABLED"
      secret_arn  = aws_secretsmanager_secret.rds_username_and_password.arn
    }
  ]
}

resource "aws_secretsmanager_secret" "rds_username_and_password" {
  name                    = "${local.datastore_db_name}-secret"
  description             = "RDS username and password"
  recovery_window_in_days = 0
  tags                    = {
      "Name" = "${local.datastore_db_name}-secret-manager"
      "nombre" = "${local.datastore_db_name}-secret-manager"
    }
}

resource "aws_secretsmanager_secret_version" "rds_username_and_password" {
  secret_id     = aws_secretsmanager_secret.rds_username_and_password.id
  secret_string = jsonencode(local.username_password)
}
